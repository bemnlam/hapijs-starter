import {
  makeExecutableSchema,
  addMockFunctionsToSchema,
  mockServer,
  MockList,
} from 'graphql-tools';
import { graphqlSchema as mockSchema } from './../src/main';
import { expect } from 'chai';
import { graphql, GraphQLSchema } from 'graphql';

class QueryTestCase {
  id: string;
  query: string;
  variables: object;
  context: object;
  expected: object;
}

const internals: { cases: Array<QueryTestCase> } = {
  cases: [],
};

internals.cases.push({
  id: 'personByPersonId',
  query: `
    query findPerson {
      personByPersonId(ids: ["foo-bar"]) {
        name {
          firstName
          lastName
        }
      }
    }
  `,
  variables: {},
  context: {},
  expected: {
    data: {
      personByPersonId: [
        {
          name: {
            firstName: 'Foo',
            lastName: 'Foo',
          },
        },
        {
          name: {
            firstName: 'Foo',
            lastName: 'Foo',
          },
        },
      ],
    },
  },
} as QueryTestCase);

internals.cases.push({
  id: 'brandByBrandId',
  query: `
    query findBrand {
      brandByBrandId(ids: ["essenceglobal"]) {
        name
        url
      }
    }
  `,
  variables: {},
  context: {},
  expected: {
    data: {
      brandByBrandId: [
        {
          name: 'Foo',
          url: 'Foo',
        },
        {
          name: 'Foo',
          url: 'Foo',
        },
      ],
    },
  },
} as QueryTestCase);

describe('GraphQL schemas', async (): Promise<void> => {

  before('construct mock schema', (done: MochaDone): void => {
    addMockFunctionsToSchema({
      schema: mockSchema,
      mocks: {
        String: () => 'Foo',
        // Query: () => ({
        //   personByPersonId: () => new MockList(1), // restricted to single item
        //   brandByBrandId: () => new MockList(1), // restricted to single item
        // }),
      },
    });

    done();
  });

  internals.cases.forEach(t => {
    const { id, query, variables, context: ctx, expected } = t;

    it(`query: ${id}`, async (): Promise<void> => {
      const result = await graphql(mockSchema, query, null, { ctx }, variables);
      await expect(result).to.deep.equal(expected);
    });
  });

});
