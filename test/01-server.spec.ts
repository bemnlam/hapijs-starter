'use strict';

import { expect } from 'chai';
import { server } from './../src/main';
import { ServerInjectOptions, ServerInjectResponse } from 'hapi';
import * as HttpStatus from 'http-status-codes';

describe('GraphQL endpoint', async (): Promise<void> => {
  const requestDefaults: ServerInjectOptions = {
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
    },
    payload: '[{ "query": "{ __schema { types { name } } }"}]',
  };

  let response: ServerInjectResponse;

  before('init server request', async (): Promise<void> => {
    response = await server.inject(requestDefaults);
  });

  it('should contain an POST endpoint /graphql', async (): Promise<void> => {
    expect(response.statusCode).to.equal(HttpStatus.OK);
    expect(response.result).to.not.be.null;
  });

  it('should return response with [{ data: {...} }] in result', () => {
    const jsonResponse: { [index: string]: any } = JSON.parse(response.result.toString());
    expect(jsonResponse).with.lengthOf(1);
    expect(jsonResponse[0]).to.have.property('data');
  });

  it('should return response with Content-Type `application/json` in header', async (): Promise<void> => {
    expect(response.headers).to.include.keys('content-type');
    expect(response.headers['content-type']).to.contains('application/json');
  });

  after('stop server', async (): Promise<void> => {
    server.stop();
  });

});
