'use strict';

import { PersonModel } from '../src/modules/person/person';
import { BrandModel } from '../src/modules/brand/brand';
import { expect } from 'chai';

describe('Mongoose schemas', (): void => {

  describe('Person', (): void => {
    const requiredFields: Array<string> = ['jobTitle', 'organization', 'avatar', 'name.firstName', 'name.lastName'];

    requiredFields.forEach( (f: string): void => {

      it(`should be invalid if ${f} is empty`, (done: MochaDone): void => {
        const p = new PersonModel();

        p.validate((err: any) => {
          expect(err.errors[f]).to.exist;
          done();
        });

      });

    });

    describe('name', (): void => {
      it('should be invalid if ONLY firstName is non-empty', (done: MochaDone): void => {
        const p = new PersonModel();
        p.name.lastName = 'foo';
        p.validate((err: any) => {
          expect(err.errors['name.firstName']).to.exist;
          expect(err.errors['name.lastName']).to.not.exist;
          done();
        });
      });

      it('should be invalid if ONLY lastName is non-empty', (done: MochaDone): void => {
        const p = new PersonModel();
        p.name.firstName = 'boo';
        p.validate((err: any) => {
          expect(err.errors['name.firstName']).to.not.exist;
          expect(err.errors['name.lastName']).to.exist;
          done();
        });
      });

      it('should be valid if firstName and lastName are non-empty', (done: MochaDone): void => {
        const p = new PersonModel();
        p.name.firstName = 'boo';
        p.name.lastName = 'foo';
        p.validate((err: any) => {
          expect(err.errors['name.firstName']).to.not.exist;
          expect(err.errors['name.lastName']).to.not.exist;
          done();
        });
      });

    });

  });

  describe('Brand', (): void => {

    const requiredFields: Array<string> = ['id', 'name'];

    requiredFields.forEach( (f: string): void => {

      it(`should be invalid if ${f} is empty`, (done: MochaDone): void => {
        const b = new BrandModel();

        b.validate((err: any) => {
          expect(err.errors[f]).to.exist;
          done();
        });

      });

    });

  });

});
