'use strict';

import { IResolvers } from 'graphql-tools';
import { PersonResolver } from '../modules/person/person.resolver';
import { EventResolver } from '../modules/event/event.resolver';
import { BrandResolver } from '../modules/brand/brand.resolver';

export const resolvers: {
  [key: string]: IResolvers<any, any>;
} = {
  PersonResolver,
  EventResolver,
  BrandResolver,
};
