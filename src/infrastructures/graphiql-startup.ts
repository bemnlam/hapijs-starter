'use strict';

export const defaultQuery: string = `
query findPerson {
  personByPersonId(ids: ["michelle-wong"]) {
    name {
      firstName
      lastName
    }
  }
}

query findBrand {
  brandByBrandId(ids: ["essenceglobal"]) {
    name
    url
  }
}

query findEvent {
  eventByName(name: "digitalmediaawards-china-2018") {
    partners {
      title
      # Brands
      brands {
        name
        url
      }
    }
    # People
    people {
      name {
        firstName
        lastName
      }
      organization
      jobTitle
      bio
      avatar
    }
    # Contacts
    contacts {
      name {
        firstName
        lastName
      }
      organization
      jobTitle
      bio
      avatar
    }
    id
    name
    startDate
    endDate
    navLogo {
      caption
      src
    }
    navigations {
      sortorder
      item {
        url
        label
        src
      }
    }
    sns {
      sortorder
      item {
        url
        label
        src
      }
    }
    jumbotron {
      logo {
        src
      }
      background {
        src
      }
      content
    }
    footer {
      logo {
        h
        w
        caption
        src
      }
      background {
        h
        w
        caption
      }
      content
    }
    intro {
      description
    }
    photos {
      src
    }
    metadata {
      sortorder
      section
    }
  }
}
`;
