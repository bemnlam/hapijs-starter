
'use strict';

import { mergeTypes } from 'merge-graphql-schemas';
import * as glob from 'glob';
import * as fs from 'fs';
import { makeExecutableSchema, ITypeDefinitions, IExecutableSchemaDefinition, IResolvers } from 'graphql-tools';
import { GraphQLSchema } from 'graphql/type';

export interface ISchemaDefinition<TContext = any> {
  typeDefs: Array<string>;
  resolvers: IResolvers<any, TContext> | Array<IResolvers<any, TContext>>;
}
export class GraphQLFactory {

  public makeGraphQLSchema<TContext = any>({ typeDefs, resolvers }: ISchemaDefinition<TContext>): GraphQLSchema {
    return makeExecutableSchema({
      typeDefs: this.mergeTypesByPaths(...typeDefs),
      resolvers,
    });
  }

  private mergeTypesByPaths(...pathsToTypes: Array<string>): ITypeDefinitions {
    return mergeTypes(...pathsToTypes.map(pattern => this.loadFiles(pattern)), { all: true });
  }

  private loadFiles(pattern: string): Array<string> {
    const paths: Array<string> = glob.sync(pattern);
    return paths.map(path => fs.readFileSync(path, 'utf8'));
  }

}
