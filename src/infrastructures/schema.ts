'use strict';

import * as mongoose from 'mongoose';
import { IMetadata } from './interfaces';

const Photo = new mongoose.Schema({
  h: { type: Number, min: 0, required: false },
  w: { type: Number, min: 0, required: false },
  src: { type: String, required: true },
  caption: { type: String, required: false },
});

const CallForAction = new mongoose.Schema({
  url: { type: String, required: true },
  label: { type: String, required: false },
  src: { type: String, required: false },
});

const Navigation = new mongoose.Schema({
  item: CallForAction,
  sortorder: { type: Number, min: 0, required: true },
});

const Jumbotron = new mongoose.Schema({
  logo: Photo,
  background: Photo,
  content: [{ type: String, required: false }],
});

const Footer = Jumbotron;

const Programme = new mongoose.Schema({
  // speakers: [{ type: String, required: false }],
  name: { type: String, required: true },
  description: [{ type: String, required: false }],
  startDate: { type: Date, required: false },
  endDate: { type: Date, required: false },
});

const Partner = new mongoose.Schema({
  title: { type: String, required: true },
  Brands: [{ type: String, required: true }],
});

const Metadata = new mongoose.Schema({
  sortorder: { type: Number, min: 0, required: true },
  section: {type: String, required: true },
});

export const Schemas = {
  Photo,
  Navigation,
  CallForAction,
  Jumbotron,
  Footer,
  Programme,
  Partner,
  Metadata,
};
