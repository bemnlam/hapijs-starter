'use strict';

import { EnumSectionType } from './enums';
import * as mongoose from 'mongoose';

// atom
export interface IMetadata {
  sortorder: number;
  section: string; // Event's property name
}

export interface ICallForAction {
  url: string;
  label: string;
  src: string;
}

export interface IPhoto {
  h: number;
  w: number;
  src: string;
  caption: string;
}

export interface IName {
  firstName: string;
  lastName: string;
}

// export interface ISection {
//   id: string,
//   as: EnumSectionType, // for casting
//   sortorder: number, // for page ordering
//   content: string | null, // id of resx linked
//   type: string | null, // mongodb collection name
// }

export interface IPartner {
  title: string;
  Brands: [string]; // Brand Id
}

// molecule
export interface IStep {
  sortorder: number;
  slug: string | null;
  callForAction: [ICallForAction] | null;
  title: string | null;
  description: string | null;
}

export interface IProgramme {
  speaker_as_People: [string]; // People id
  name: string;
  description: [string];
  startDate: Date | null;
  endDate: Date | null;
}

// organisms
export interface INavigation {
  item: ICallForAction;
  sortorder: number;
}

export interface IJumbotron {
  logo: IPhoto;
  background: IPhoto;
  content: [string];
}

export interface IFooter {
  logo: IPhoto;
  content: [string];
}

// template
