'use strict';

import * as mongoose from 'mongoose';
import { Schema as EventSchema } from './schema';
import {
  IFooter,
  IJumbotron,
  IMetadata,
  INavigation,
  IPartner,
  IPhoto,
  IProgramme,
  IStep,
} from '../../infrastructures/interfaces';

export interface IEvent {
  id: string;
  navLogo: IPhoto;
  year: number;
  name: string;
  url: string;
  startDate: Date;
  endDate: Date | null;
  navigations: INavigation[];
  sns: INavigation[];
  jumbotron: IJumbotron;
  footer: IFooter;
  intro: IProgramme;
  photos: IPhoto;
  people: string[];
  contacts: string[];
  partners: IPartner[];
  programmes: IProgramme[];
  guildlines: IStep[];

  metadata: IMetadata[];
  // sections: ISection[],
  // TODO: more event fields
}

export type Event = IEvent & mongoose.Document;
export const EventModel: mongoose.Model<Event> = mongoose.model<Event>('event', EventSchema);
