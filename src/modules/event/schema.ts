'use strict';

import * as mongoose from 'mongoose';
import { Schemas } from '../../infrastructures/schema';
import { Schema as BrandSchema } from '../brand/schema';

export const Schema = new mongoose.Schema({
  id: { type: String, required: true, unique: true },
  navLogo: Schemas.Photo,
  year: { type: Number, min: 1970, max: 9999, required: true },
  name: { type: String, required: true },
  url: { type: String, required: true },
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: false },
  navigations: [Schemas.Navigation],
  sns: [Schemas.Navigation],
  jumbotron: Schemas.Jumbotron,
  footer: Schemas.Jumbotron,
  intro: Schemas.Programme,
  photos: [Schemas.Photo],
  People: [{ type: String, required: false}], // personId
  Contacts: [{ type: String, required: false}], // personId
  partners: [Schemas.Partner],
  metadata: [Schemas.Metadata],
});
