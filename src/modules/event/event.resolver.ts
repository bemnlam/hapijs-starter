'use strict';

import { EventModel } from './event';
import { IResolvers } from 'graphql-tools';

export const EventResolver: IResolvers<any, any> = {
  Query: {
    eventByName(root, { name }) {
      // console.log(name);
      return EventModel.findOne({
        id: name,
      })
      .then(response => response);
    },
  },
};
