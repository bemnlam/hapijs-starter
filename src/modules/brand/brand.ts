'use strict';

import * as mongoose from 'mongoose';
import { Schema as BrandSchema } from './schema';
import {
  IFooter,
  IJumbotron,
  IMetadata,
  INavigation,
  IPartner,
  IPhoto,
  IProgramme,
  IStep,
} from '../../infrastructures/interfaces';

export interface IBrand {
  id: string;
  name: string;
  url: string;
  src: string;
}

export type Brand = IBrand & mongoose.Document;
export const BrandModel: mongoose.Model<Brand> = mongoose.model<Brand>('brand', BrandSchema);
