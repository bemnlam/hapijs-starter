'use strict';

import { BrandModel, IBrand } from './brand';
import { IResolvers } from 'graphql-tools';

export const BrandResolver: IResolvers<any, any> = {
  Query: {
    async brandByBrandId(root, { ids }): Promise<IBrand[]> {
      // console.log(`brandId: ${ids}`);
      return await BrandModel.find()
      .where('id')
      .in(ids)
      .exec();
    },
  },
};
