'use strict';

import * as mongoose from 'mongoose';

export const Schema = new mongoose.Schema({
  id: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  src: { type: String, required: false},
  url: { type: String, required: false},
});
