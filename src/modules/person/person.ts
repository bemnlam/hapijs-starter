'use strict';

import * as mongoose from 'mongoose';
import { Schema as PersonSchema } from './schema';
import { IName } from '../../infrastructures/interfaces';

export interface IPerson {
  name: IName;
  jobTitle: string;
  organization: string;
  avatar: string;
  bio: string;
}

// let p = <IPerson> {
//   name: <IName> {
//     firstName: 'foo',
//     lastName: 'bar'
//   },
//   jobTitle: ''
// }

export type Person = IPerson & mongoose.Document;
export const PersonModel: mongoose.Model<Person> = mongoose.model<Person>('person', PersonSchema);
