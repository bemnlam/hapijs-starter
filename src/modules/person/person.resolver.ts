'use strict';

import { PersonModel, IPerson } from './person';
import { IResolvers } from 'graphql-tools';

// export const firstnameResolver = {
//   Query: {
//     getPersonByFirstname(root, { name }) {
//       console.log(name)
//       return PersonModel.findOne({ 'name.firstName': name }).then(response => response);
//     }
//   }
// };

// export const lastnameResolver = {
//   Query: {
//     getPersonByLastname(root, { name }) {
//       console.log(name)
//       return PersonModel.findOne({ 'name.lastName': name }).then(response => response);
//     }
//   }
// };

export const PersonResolver: IResolvers<any, any> = {
  Query: {
    async personByPersonId(root, { ids }): Promise<IPerson[]> {
      // console.log(`personId: ${ids}`);
      return await PersonModel.find()
      .where('id')
      .in(ids)
      .exec();
    },
  },
};
