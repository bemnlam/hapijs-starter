'use strict';

import * as mongoose from 'mongoose';
import { Schemas } from '../../infrastructures/schema';

export const Schema = new mongoose.Schema({
  name: {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
  },
  jobTitle: { type: String, required: true },
  organization: { type: String, required: true },
  avatar: { type: String, required: true },
  bio: { type: String },
});
