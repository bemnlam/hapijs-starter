'use strict';

import * as Hapi from 'hapi';
import * as mongoose from 'mongoose';
// app config
import config from './../config';
import * as dotenv from 'dotenv-safe';
// import * as dotenv from 'dotenv-safe';
// graphql
import { graphqlHapi, graphiqlHapi, HapiPluginOptions, HapiGraphiQLPluginOptions } from 'apollo-server-hapi';
import { mergeSchemas, MergeInfo, IResolvers } from 'graphql-tools';
import { GraphQLFactory } from './infrastructures/graphQLFactory';
import { GraphQLSchema } from 'graphql/type';
import { defaultQuery } from './infrastructures/graphiql-startup';
import { resolvers } from './infrastructures/resolvers';

class Internals {
  graphQLFactory: GraphQLFactory;
  server: Hapi.Server;
  resolvers: Array<IResolvers<any, any>>;
  graphqlSchema: GraphQLSchema;
  init: () => Promise<void>;
}

const internals = new Internals();

internals.graphQLFactory = new GraphQLFactory();

internals.server = new Hapi.Server(config.hapi.server);

internals.resolvers = Object.keys(resolvers).map((key: string): IResolvers<any, any> => resolvers[key]);

internals.graphqlSchema = internals.graphQLFactory.makeGraphQLSchema({
  typeDefs: config.graphql.typeDefPattern,
  resolvers: internals.resolvers,
});

internals.graphqlSchema = mergeSchemas({
  schemas: [internals.graphqlSchema],
  resolvers: (mergeInfo: MergeInfo): IResolvers => ({
    Partner: {
      brands: {
        fragment: `fragment BrandFragment on Partner { Brands }`,
        resolve(parent, args, context, info) {
          // console.log(parent.Brands);
          return mergeInfo.delegate(
            'query',
            'brandByBrandId',
            {
              ids: parent.Brands,
            },
            context,
            info,
          );
        },
      },
    },
  }),
});

internals.graphqlSchema = mergeSchemas({
  schemas: [internals.graphqlSchema],
  resolvers: (mergeInfo: MergeInfo): IResolvers => ({
    Event: {
      people: {
        fragment: `fragment EventFragment on Event { People }`,
        resolve(parent, args, context, info) {
          return mergeInfo.delegate(
            'query',
            'personByPersonId',
            {
              ids: parent.People,
            },
            context,
            info,
          );
        },
      },
      contacts: {
        fragment: `fragment EventFragment on Event { Contacts }`,
        resolve(parent, args, context, info) {
          return mergeInfo.delegate(
            'query',
            'personByPersonId',
            {
              ids: parent.Contacts,
            },
            context,
            info,
          );
        },
      },
    },
  }),
});

mongoose.connect(config.mongoose.connectionString);

if (!config.isProduction) {

  internals.server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      const envConfig = dotenv.config();
      return envConfig.parsed;
    },
  } as Hapi.ServerRoute);

}

internals.init = async (): Promise<void> => {

  await internals.server.register<HapiPluginOptions>({
    plugin: graphqlHapi,
    options: config.hapi.plugin.graphqlHapi(internals.graphqlSchema),
  });

  if (config.graphql.allowGraphiQL) {

    await internals.server.register<HapiGraphiQLPluginOptions>({
      plugin: graphiqlHapi,
      options: config.hapi.plugin.graphiqlHapi(defaultQuery),
    });
  }

  await internals.server.start();
  // tslint:disable-next-line:no-console
  console.log(`GraphiQL running at: ${internals.server.info.uri}/graphiql`);
};

process.on('unhandledRejection', (err: any): void => {
  // tslint:disable-next-line:no-console
  console.log(err);
  process.exit(1);
});

internals.init();

export const server: Hapi.Server = internals.server;
export const graphqlSchema: GraphQLSchema = internals.graphqlSchema;
