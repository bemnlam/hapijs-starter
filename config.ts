'use strict';

// interfaces
import { ServerOptions } from 'hapi';
import { HapiPluginOptions, HapiGraphiQLPluginOptions } from 'apollo-server-hapi';
import { CacheControlExtensionOptions } from 'apollo-cache-control';
import { GraphQLSchema } from 'graphql';
import { GraphQLOptions } from 'apollo-server-core';
import { GraphiQLData } from 'apollo-server-module-graphiql';
// env variables
import * as dotenv from 'dotenv-safe';
import { DotenvResult } from 'dotenv';

interface IHapiConfig {
  server: ServerOptions;
  plugin: {
    graphqlHapi: (graphQLschema: GraphQLSchema) => HapiPluginOptions,
    graphiqlHapi: (query: string) => HapiGraphiQLPluginOptions,
    [key: string]: any,
  };
  [key: string]: any;
}

interface IGraphQLConfig {
  typeDefPattern: Array<string>;
  allowGraphiQL: boolean;
  [key: string]: any;
}

interface IMongooseConfig {
  connectionString: string;
  [key: string]: any;
}

interface IConfig {
  environment: string;
  isProduction: boolean;
  hapi: IHapiConfig;
  graphql: IGraphQLConfig;
  mongoose: IMongooseConfig;
  [key: string]: any;
}

const envConfig: DotenvResult = dotenv.config();
if (envConfig.error) {
  throw envConfig.error;
}

const environment: string = process.env.NODE_ENV || 'development';
const isProduction: boolean = process.env.NODE_ENV === process.env.ENV_PROD;

const hapi: IHapiConfig = {
  server: {
    host: process.env.SERVER_HOST,
    port: process.env.SERVER_PORT,
  } as ServerOptions,
  plugin: {
    graphqlHapi(graphQLschema: GraphQLSchema): HapiPluginOptions {
      return {
        path: '/graphql',
        graphqlOptions: {
          schema: graphQLschema,
          debug: process.env.API_ALLOW_DEBUG === process.env.ASSERT_TRUE,
          tracing: process.env.API_ALLOT_TRACING === process.env.ASSERT_TRUE,
          cacheControl: (process.env.API_ALLOW_CACHE === process.env.ASSERT_TRUE) && (!isNaN(parseInt(process.env.API_TTL, 10))) ? {
            defaultMaxAge: parseInt(process.env.API_TTL, 10),
          } as CacheControlExtensionOptions : false,
        } as GraphQLOptions,
        route: {
          cors: process.env.API_ALLOW_CORS === process.env.ASSERT_TRUE,
        },
      } as HapiPluginOptions;
    },
    graphiqlHapi(query: string): HapiGraphiQLPluginOptions {
      return {
        path: '/graphiql',
        graphiqlOptions: {
          endpointURL: '/graphql',
          query,
          editorTheme: 'monokai',
        } as GraphiQLData,
      } as HapiGraphiQLPluginOptions;
    },
  },
};

const graphql: IGraphQLConfig = {
  typeDefPattern: ['./**/*.graphql'],
  allowGraphiQL: !isProduction && (process.env.API_ALLOW_GRAPHIQL === process.env.ASSERT_TRUE),
};

// todo: test this function
const getConnectionString = (): string => {
  let url = '';
  const params = [];

  if (process.env.DB_USER && process.env.DB_PASSWORD) {
    url = `mongodb://${ process.env.DB_USER }:${ process.env.DB_PASSWORD }@${ process.env.DB_HOST }/${ process.env.DB_NAME }`;

    if (process.env.DB_AUTH_SOURCE) {
      params.push(`ssl=true`);
      params.push(`authSource=${ process.env.DB_AUTH_SOURCE }`);
    }
  } else {
    url = `mongodb://${ process.env.DB_HOST }/${ process.env.NAME }`;
  }

  if (process.env.DB_REPLICA_SET) {
    params.push(`replicaSet=${ process.env.DB_REPLICA_SET }`);
  }

  const param = (params.length > 0) ? `?${ params.join('&') }` : '';
  const result = url + param;

  return result;
};

const mongoose: IMongooseConfig = {
  connectionString: getConnectionString(),
  // connectString: 'mongodb://mongodb_user:mongodb_user@cluster0-shard-00-00-hdods.mongodb.net:27017,cluster0-shard-00-01-hdods.mongodb.net:27017,cluster0-shard-00-02-hdods.mongodb.net:27017/HaymarketEvent?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin',
};

export default {
  environment,
  isProduction,
  hapi,
  graphql,
  mongoose,
} as IConfig;
