// Type definitions for merge-graphql-schema v1.5.1
// Project: https://github.com/okgrow/merge-graphql-schemas.git


declare module "merge-graphql-schemas" {
  interface IMergeOptions {
    all: boolean
  }

  type IMergeTypesOptions = IMergeOptions | string[];
  function mergeTypes(
    ...types: IMergeTypesOptions[],
    // options?: IMergeOptions,
  ): string;

  function mergeResolvers<T>(
    resolvers: T | any[] ,
  ): any

  interface IFileLoaderOptions {
    recursive: boolean,
    extensions: string[],
    globOptions: any,
  }
  function fileLoader(
    folderPath: string,
    fileLoaderOptions: IFileLoaderOptions
  ): string[];

  export {
    mergeResolvers,
    mergeTypes,
    fileLoader,
   };
}
