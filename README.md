# Haymarket Event Server (wip)

A graphQL server prototype using [**Hapi**](https://hapijs.com/), [**typescript**](https://www.typescriptlang.org/), [**Apollo Server**](https://github.com/apollographql/apollo-server), [**mongoose**](http://mongoosejs.com/)

## _(NOTE: `hapijs` requires Node.js version >= `8.9.0`. You may need to update Node.js)_
[Download latest Node.js](https://nodejs.org/en/) 
## _(NOTE: make sure that MongoDB Atlas whitelisted the ip of your device. Otherwise you cannot connect to the db.)_
**(Let mongoDB Atlas account owner to whitelist you first)**

## Set up


```
yarn install
```

### This project uses [`dotenv-safe`](https://github.com/rolodato/dotenv-safe) to manage environment variables.
To set up environment variables: 

1. copy `.env.example` and rename it into `.env`
2. fill the values in `.env`

### start the site at **http://localhost:3001** with watch:
```
yarn run dev
```

### just run, no watch:
```
yarn run dev:node
```


(* try to remove cookies of `localhost` if a error 400 `Invalid cookie value` message appears)

## Run Production

```
yarn run build
yarn run start
```

## GraphQL playground

**http://localhost:3001/graphiql** (yes, it's graphiql, not graphql)

an example query to find a person:
``` graphql
query findPerson {
  personByPersonId(ids: ["michelle-wong"]) {
    name {
      firstName
      lastName
    }
  }
}
```

## GraphQL request (wip)
- use [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=zh-TW) to make a POST request with `Content-Type` as `application/json`  to **http://localhost:3001/graphql**
```
[{
  "query": "query findPerson { personByPersonId(ids: [\"michelle-wong\"]) { name { firstName, lastName }}}"
}]
```


## GraphQL schemas and types
For merged schema, see [`/migration/schema.gql`](./migration/schema.gql).

Assuming graphql endpoint is http://localhost:3001/graphql and the server is running, you can generate merged schema by: 

```
# start the server
yarn run dev
```
```
# ...in another terminal
yarn run schema
```


## GraphQL cheatsheet
(from https://github.com/sogko/graphql-schema-language-cheat-sheet)

![GraphQL Schema Language Cheat Sheet](https://raw.githubusercontent.com/sogko/graphql-shorthand-notation-cheat-sheet/master/graphql-shorthand-notation-cheat-sheet.png)


## Testing

Using mocha + chai

```
yarn test
```
